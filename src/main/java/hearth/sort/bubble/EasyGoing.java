package hearth.sort.bubble;

import java.util.Scanner;

public class EasyGoing {

/*
input
1
5 1
1 2 3 4 5

output
4
*/

	public static void bubbleSort(int a[]) {
		for (int i = 0; i < a.length - 1; i++) {
			for (int j = 0; j < a.length - i - 1; j++) {
				if (a[j] > a[j + 1]) {
					// swap
					int x = a[j];
					a[j] = a[j + 1];
					a[j + 1] = x;
				}
			}
		}
	}
	
	public static int sumArray(int a[], int from, int to) {
		int sum = 0;
        for (int i = from; i < to; i++) {
        	sum += a[i];
        }
        return sum;
	}

	public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        
        for (int i = 0; i < t; i++) {
            int n = sc.nextInt();
            int m = sc.nextInt();
            int a[] = new int[n];
            for (int j = 0; j < n; j++) {
            	a[j] = sc.nextInt();
            }
            bubbleSort(a);
            int min = sumArray(a, 0, m);
            int max = sumArray(a, n - m, n);
            System.out.println(max - min);
        }
        sc.close();
	}

}
