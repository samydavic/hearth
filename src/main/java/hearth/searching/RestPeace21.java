package hearth.searching;

import java.util.Scanner;

public class RestPeace21 {

/*
input
1
69521

output
The streak lives still in our heart!
 */
	public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        sc.nextLine();
        for (int i = 0; i < n; i++) {
            String s = sc.nextLine();
            if (s.contains("21") || (Integer.parseInt(s) % 21 == 0)) {
				System.out.println("The streak is broken!");
			} else {
				System.out.println("The streak lives still in our heart!");
			}
        }
        sc.close();
	}

}
