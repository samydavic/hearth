package hearth.searching;

import java.util.Scanner;

public class MonkTakesWalk {

/*
input
2
nBBZLaosnm
JHkIsnZtTL

output
2
1
 */
	public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
		String vowels =  "AEIOUaeiou";

        int n = sc.nextInt();
        sc.nextLine();
        for (int i = 0; i < n; i++) {
            String s = sc.nextLine();
            char[] array = s.toCharArray();
            int counter = 0;
            for (int j = 0; j < array.length; j++) {
            	if (vowels.indexOf(array[j]) >= 0) {
            		counter++;
            	}
            }
            System.out.println(counter);;
            
        }
        sc.close();

	}

}
