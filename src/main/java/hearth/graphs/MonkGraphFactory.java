package hearth.graphs;

import java.util.Scanner;

public class MonkGraphFactory {

/*
input
3
1 2 1

output
YES
*/
	
	public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int degrees = 0;
        for (int i = 0; i < n; i++) {
            degrees += sc.nextInt();
        }
        sc.close();
        
		if ((2 * (n - 1) == degrees)) {
			System.out.println("Yes");
		} else {
			System.out.println("No");
		}
	}
}
