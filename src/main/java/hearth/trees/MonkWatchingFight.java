package hearth.trees;

import java.util.Scanner;

public class MonkWatchingFight {

/*
input
4
2 1 3 4

output
3
*/
	
	static class Node {
		int data;
		Node right;
		Node left;
		Node(int value) {
			this.data = value;
		}
	}
	
	static Node insert(Node root, int value) {
		if (root == null) {
			return new Node(value);
		} else if (value <= root.data) {
			root.left = insert(root.left, value);
		} else {
			root.right = insert(root.right, value);
		}
		return root;
	}
	
	static int height(Node root) {
		if (root == null) {
			return 0;
		} else {
			int lHeight = height(root.left);
			int rHeight = height(root.right);
			
			if (lHeight > rHeight) {
				return lHeight + 1;
			} else {
				return rHeight + 1;
			}
		}
		
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		
		Node root = null;
		for (int i = 0; i < n ; i++) {
			int value = sc.nextInt();
			root = insert(root, value);
		}
		
		System.out.println(height(root));
		sc.close();
	}

}
