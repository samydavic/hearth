package hearth.sort.bubble;

import java.util.Scanner;

public class SavePatients {

/*
input
5
123 146 454 542 456
100 328 248 689 200

output
No
*/

	public static void bubbleSort(int a[]) {
		for (int i = 0; i < a.length - 1; i++) {
			for (int j = 0; j < a.length - i - 1; j++) {
				if (a[j] > a[j + 1]) {
					// swap
					int x = a[j];
					a[j] = a[j + 1];
					a[j + 1] = x;
				}
			}
		}
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int a[] = new int[n];
		int b[] = new int[n];

		for (int i = 0; i < a.length; i++) {
			a[i] = sc.nextInt();
		}

		for (int i = 0; i < a.length; i++) {
			b[i] = sc.nextInt();
		}
		sc.close();

		bubbleSort(a);
		bubbleSort(b);

		for (int i = 0; i < a.length; i++) {
			if (a[i] < b[i]) {
				System.out.println("No");
				return;
			}
		}
		System.out.println("Yes");
	}

}
