package hearth.search;

import java.util.Scanner;

public class TestUnderstanding {

/*

input
5
1 2 3 4 5
5
1
2
3
4
5

output
1
2
3
4
5

*/
	
	public static int binarySearch(int[] a, int key) {
		int low = 0;
		int high = a.length;

		while (low <= high) {
			int mid = (low + high) / 2;
			if (a[mid] < key) {
				low = mid + 1;
			} else if (a[mid] > key) {
				high = mid - 1;
			} else {
				return mid;
			}
		}
		return -1;
		
	}

	public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int a[] = new int[n];
        
        for (int i = 0; i < n; i++) {
        	a[i] = sc.nextInt();
        }

        int q = sc.nextInt();
        for (int i = 0; i < q; i++) {
        	int key = sc.nextInt();
        	int pos = binarySearch(a, key);
        	System.out.println(pos);
        }
        sc.close();

	}

}
