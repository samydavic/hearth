package hearth.graphs;

import java.util.Scanner;

public class EdgeExistence {
/*
input
3 3
1 2
3 1
2 3
1
1 3

output

 */
	
	public static void main(String[] args) {
        //Scanner
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int m = s.nextInt();
        boolean[][] matrix = new boolean[n][n];
        for (int i = 0; i < m; i++) {
            int a = s.nextInt();
            int b = s.nextInt();
            matrix[a - 1][b - 1] = true;
        }

        int q = s.nextInt();
        for (int i = 0; i < q; i++) {
            int a = s.nextInt();
            int b = s.nextInt();
            if (matrix[a - 1][b - 1]) {
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }
        }
        s.close();
	}

}
