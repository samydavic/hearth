package hearth.trees;

import java.util.Scanner;

public class CreateBST {

/*
input
4
2 1 3 4
3

output
3
4

*/
	
	static class Node {
		int data;
		Node left;
		Node right;
		public Node(int value) {
			this.data = value;
		}
	}
	
	static Node insert(Node root, int value) {
		if (root == null) {
			return new Node(value);
		} else {
			if (value <= root.data) {
				root.left = insert(root.left, value);
			} else {
				root.right = insert(root.right, value);
			}
			return root;
		}
	}
	
	static void preorder(Node root) {
		if (root != null) {
			System.out.println(root.data);
			preorder(root.left);
			preorder(root.right);
		}
	}

	public static Node search(Node root, int value) {
		if (value == root.data) {
			return root;			
		} else if (value < root.data) {
			return search(root.left, value);
		} else if (value > root.data) {
			return search(root.right, value);
		}
		return null;		
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		
		Node root = null;
		for (int i = 0; i < n; i++) {
			int value = sc.nextInt();
			root = insert(root, value);
		}
		
		Node found = search(root, sc.nextInt());
		preorder(found);

		sc.close();
	}

}
