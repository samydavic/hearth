package hearth.graphs;

import java.util.*;

public class MonkRealState {
/*
input
1
3
1 2
2 3
1 3

output
3
 */
	public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        for (int i = 0; i < t; i++) {
            int n = sc.nextInt();
            Set<Integer> nodes = new HashSet<Integer>();
            for (int j = 0; j < n; j++) {
                int x = sc.nextInt();
                int y = sc.nextInt();
                nodes.add(x);
                nodes.add(y);                
            }
            System.out.println(nodes.size());
        }
        sc.close();
	}

}
