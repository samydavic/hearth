package hearth.trees;

import java.util.Scanner;

public class DiameterBinaryTree {

/*

input
5 1
L
2
R
3
LL
4
LR
5

output
4
*/

	static class Node {
		int value;
		Node left;
		Node right;
		Node(int value) {
			this.value = value;
		}
	}
	
	public static void insert(Node root, String path, int value) {
		if (path.charAt(0) == 'L') {
			if (path.length() == 1) {
				root.left = new Node(value);
			} else {
				insert(root.left, path.substring(1), value);
			}
		} else if (path.charAt(0) == 'R') {
			if (path.length() == 1) {
				root.right = new Node(value);
			} else {
				insert(root.right, path.substring(1), value);
			}			
		}
	}
	
	public static int maxDeep(Node node) {
		if (node == null) {
			return 0;
		}
		
		int lDeep = maxDeep(node.left);
		int rDeep = maxDeep(node.right);
		
		if (lDeep > rDeep) {
			return lDeep + 1;			
		} else {
			return rDeep + 1;
		}
	}
	
	public static int diameter(Node node) {
		if (node != null) {
			int lHight = maxDeep(node.left);
			int rHight = maxDeep(node.right);
			
			int lDiameter = diameter(node.left);
			int rDiameter = diameter(node.right);
			
			return Math.max(lHight + rHight + 1, Math.max(lDiameter, rDiameter));
		}
		return 0;		
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int t = sc.nextInt();
		int x = sc.nextInt();
		
		Node root = new Node(x);		
		for (int i = 0; i < t -1; i++) {
			sc.nextLine();
			String path = sc.nextLine();
			int value = sc.nextInt();
			insert(root, path, value);
		}
		sc.close();
		
		System.out.println(diameter(root));
	}

}
