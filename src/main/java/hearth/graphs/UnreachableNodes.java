package hearth.graphs;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class UnreachableNodes {

/*
input
10 10
8 1
8 3
7 4
7 5
2 6
10 7
2 8
10 9
2 10
5 10
2

output
0
*/

    static List<List<Integer>> adj;
	static boolean[] visited;
	
	private static void dfs(int s) {
		visited[s] = true;
		for (int i = 0; i < adj.get(s).size(); i++) {
			if (visited[adj.get(s).get(i)] == false)
				dfs(adj.get(s).get(i));
		}		
	}


	public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();

        // n + 1 elements to simplify computation, first element will be ignored
        adj = new ArrayList<List<Integer>>();
        for (int i = 0; i <= n; i++) {
        	adj.add(new ArrayList<Integer>());
        }

        for (int i = 0; i < m; i++) {
            int a = sc.nextInt();
            int b = sc.nextInt();
            adj.get(a).add(b);
            adj.get(b).add(a);
        }

        int x = sc.nextInt();
        sc.close();
        
        visited = new boolean[n + 1];

        dfs(x);
        
        int connected = 0;
		for (int i = 1; i <= n; i++) {
			if (visited[i]) {
				connected++;
			}
		}
		
		int unreachables = visited.length - connected -1;

		System.out.println(unreachables);
	}


}
